import App from "./app";
import CampaingControll from './controlls/campaingControll';
import BrochureController from './controlls/brocureControll';
import PayLevelController from './controlls/payLevelControll';
import CampaignSalesController from './controlls/campaignSalesControll';

const PORT = 3200;

const app = new App(
    [
      new CampaingControll(),
      new BrochureController(),
      new PayLevelController(),
      new CampaignSalesController(),
    ],
  );

  app.listen();