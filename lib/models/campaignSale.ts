
import * as mongoose from 'mongoose';
import CampaignSaleModel from 'interfaces/campainSaleInterface';

const CampaignSalesSchema = new mongoose.Schema({
    productCode: { type: String, required: true },
    salePrice: { type: String, required: true },
    purchasePrice: { type: String, required: true },
    percentageProfit: { type: String, required: false },
    clientsName: { type: String },
    ClientsPhone: { type: String, required: false },

    dateCreated: { type: Date, default: Date.now },
    dateUpdate: { type: Date, required: false },
    userEdited: { type: String, required: false }
})

const CampaignSalesCollection = mongoose.model<CampaignSaleModel & mongoose.Document>('CampaignSales', CampaignSalesSchema);

export default CampaignSalesCollection;