import * as mongoose from 'mongoose';
import PayLevelModel from '../interfaces/payLevelModel';

const PayLevelSchema = new mongoose.Schema({
    name: String,
    againPercentage: Number,

    dateCreated: { type: Date, default: Date.now },
    dateUpdate: { type: Date, required: false },
    userEdited: { type: String, required: false },
})

const PayLevelCollection = mongoose.model<PayLevelModel & mongoose.Document>('PagoNivel', PayLevelSchema);
export default PayLevelCollection;
