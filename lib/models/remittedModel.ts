import * as mongoose from 'mongoose';
import RemmitedModel from 'interfaces/remittedInterface';

const RemmitedScheme = new mongoose.Schema({

    brochureId: { type: String, required: true },
    name: { type: String, required: true },
    phone: { type: String, required: true },
    address: { type: String, required: false },

    dateCreated: { type: Date, default: Date.now },
    dateUpdate: { type: Date, required: false },
    userEdited: { type: String, required: false }
})

const RemmitedCollection = mongoose.model<RemmitedModel & mongoose.Document>('Referidos', RemmitedScheme);
export default RemmitedCollection;