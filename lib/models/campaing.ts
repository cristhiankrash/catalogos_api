
import * as mongoose from 'mongoose';
import CampaingModel from 'interfaces/campaingInterface';

const CampaingSchema = new mongoose.Schema({
    brochure: {type: String, required: true},
    dateInit: {type:Date, required: true},
    dateEnd: {type:Date, required: true},
    active: { type: Boolean, default: false },
    campaignSale: { type: Array, required: false },
    remitted: { type: Array, required: false }, //Remitidos
    Profit: { type: String, required: false },
    payLevel: { type: Object, required: false },
    sales: { type: Array, required: false },

    dateCreated: { type: Date, default: Date.now },
    dateUpdate: { type: Date, required: false },
    userEdited: { type: String, required: false },
})

const CampaingCollection = mongoose.model<CampaingModel & mongoose.Document>('Campañas', CampaingSchema);

export default CampaingCollection;