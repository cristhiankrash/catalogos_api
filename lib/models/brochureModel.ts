import * as mongoose from 'mongoose';
import BrochureModel from 'interfaces/brochureInterface';

const BrochureSchema = new mongoose.Schema({
    name:  { type: String, required: true },
    cordinaterName:{ type: String, required: true },
    cordinaterPhone: { type: String, required: true },
    addressBussines: { type: String, required: false },

    campaings: { type: [], required: false },
    payRemmitted: { type: String, required: false },
    level: { type: Object, required: false },

    dateCreated: { type: Date, default: Date.now },
    dateUpdate: { type: Date, required: false },
    userEdited: { type: String, required: false },
})

const BrochureCollection = mongoose.model<BrochureModel & mongoose.Document>('Catalogos', BrochureSchema);

export default BrochureCollection;

// var userschema = mongoose.Schema({
//     org: String,
//     username: String,
//     fullname: String,
//     password: {
//         type: String,
//         required: function(){
//             return this.email? true : false 
//         }
//     },
//     email: String
// });