import AuditModel from "./audit";

class CampaignSalesModel extends AuditModel {
    productCode?: String;
    description?: String;
    salePrice?: String;
    purchasePrice?: String;
    percentageProfit?: String;
    clientsName?: String;
    ClientsPhone?: String;
}

export default CampaignSalesModel;