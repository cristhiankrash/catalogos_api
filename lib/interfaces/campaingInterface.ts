import AuditModel from './audit';
import CampaignSaleModel from './campainSaleInterface';
import RemmitedModel from './remittedInterface'
import PayLevelModel from './payLevelModel';

class CampaingModel extends AuditModel {
    brochure: String;
    dateInit: Date;
    dateEnd: Date;
    active: Boolean;
    campaignSale?: Array<CampaignSaleModel>;
    remitted?: Array<RemmitedModel>; //Remitidos
    Profit?: String; // ganancia
    payLevel?: PayLevelModel;
    sales?: Array<CampaignSaleModel>;
}

export default CampaingModel;