import AuditModel from "./audit";

class PayLevelModel extends AuditModel {
    name: String;
    againPercentage: Number;
}

export default PayLevelModel;