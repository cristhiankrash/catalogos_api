import AuditModel from "./audit";

class BrochureModel extends AuditModel {
    name: String;
    cordinaterName: String;
    cordinaterPhone: String;
    addressBussines?: String;
    campaings?: Array<String>;
    payRemmitted?: String;
    level?: String;
}

export default BrochureModel;