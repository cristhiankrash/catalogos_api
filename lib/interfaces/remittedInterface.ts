import AuditModel from "./audit";

class RemmitedModel extends AuditModel {
    brochureId: String;
    name: String;
    phone: String;
    address: String;
}

export default RemmitedModel;