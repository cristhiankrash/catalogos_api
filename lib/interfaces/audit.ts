class AuditModel {
    dateCreated?: Date;
    dateUpdate?: Date;
    userEdited?: String;
}

export default AuditModel;