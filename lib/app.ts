import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose';
import errorMiddleware from './middleware/error.middleware';
import Controller from 'interfaces/controller.interface';

// enviroment variables
import 'dotenv/config';

class App {
    public app: express.Application;

    constructor(controllers: Controller[]) {
        this.app = express();
        this.initializeMiddlewares();
        this.connectToTheDatabase();
        this.initializeControllers(controllers);
        this.initializeErrorHandling();
    }

    private initializeMiddlewares() {
        // this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
    }

    public listen() {
        this.app.listen(process.env.SERVER_PORT, () => {
            console.log(`App listening on the port ${process.env.SERVER_PORT}`);
        })
    }

    public getServer() {
        return this.app;
    }

    private initializeControllers(controllers) {
        controllers.forEach((controller) => {
            this.app.use('/', controller.router);
        });
    }

    private connectToTheDatabase() {
        const {
            MONGO_USER,
            MONGO_PASSWORD,
            MONGO_PATH,
        } = process.env;
        // mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}${MONGO_PATH}`);
        console.log('Connection db ok')
        mongoose.connect('mongodb://localhost:27017/Catalogos')
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }
}

export default App;