import * as express from 'express';
import PayLevelCollection from '../models/payLevel';
import Controller from 'interfaces/controller.interface';
import PayLevelModel from 'interfaces/payLevelModel';

class PayLevelController implements Controller {
    public path = '/pagos/tabla';
    public router = express.Router();
    private payLevel = PayLevelCollection;

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(this.path, this.getAll);
        this.router
            .put(`${this.path}`, this.update)
            .delete(`${this.path}`, this.delete)
            .post(this.path, this.create)
        // .put(`${this.path}/:id`, this.update)
    }

    private getAll = (req: express.Request, res: express.Response) => {
        let id = req.query.id;
        if (id) {
            PayLevelCollection.findOne({ _id: id })
                .then(result => {
                    if (result) {
                        res.status(200).send(result);
                    } else {
                        res.status(404).send("Not foud");
                    }
                })
                .catch(e => {
                    res.status(500).send(`Error: ${e}`);
                })
        } else {
            PayLevelCollection.find()
                .then(result => {
                    res.status(200).send(result);
                })
                .catch(e => {
                    res.status(500).send(`Error: ${e}`);
                })
        }
    }

    private create = (req: express.Request, res: express.Response) => {
        const payLevel: PayLevelModel = req.body;
        const createPayLevel = new this.payLevel(payLevel)
        createPayLevel.save()
            .then(result => {
                res.status(201).send(result)
            })
            .catch(error => {
                res.status(500).send(`error ${error}`)
            });
    }

    private update = (req: express.Request, res: express.Response) => {
        const id = req.query.id;
        const payLevel = req.body;
        if (id) {
            this.payLevel.findByIdAndUpdate(id, payLevel, { new: true })
                .then(response => {
                    res.status(200).send(response)
                })
                .catch(e => {
                    res.status(500).send(`Error: ${e}`)
                });
        } else {
            res.send("Debe enviar un id")
        }
    }

    private delete = async (request: express.Request, response: express.Response, next?: express.NextFunction) => {
        const id = request.query.id;
        if(id){
            this.payLevel.findByIdAndDelete(id)
                .then(resp => {
                    resp ? response.status(200).send("deleted") : response.send(404);
                })
                .catch(e => {
                    response.status(500).send(`Error: ${e}`);
                });
        }
    }
}

export default PayLevelController;